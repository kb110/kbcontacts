# KBContacts

<b>Setup</b>
You need to have node 10.xx.xx installed to install node visit: https://nodejs.org/en/
To install angular cli and create a new package follow the instructions on the angular site: https://angular.io/guide/quickstart
To install angular material follow instructions on: https://material.angular.io/guide/getting-started

<b>To get started</b>
In the terminal window type <em>git clone https://kb110@bitbucket.org/kb110/kbcontacts.git</em> to clone this repo

cd into kbcontact-web
then type <em>npm i</em> to install angular packages
to run the application type <em>ng serve -o</em>
