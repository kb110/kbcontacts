﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KBContacts.Core.DAO
{
    public class LabelDAO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ContactCount { get; set; }
    }
}