﻿using KBContact.Core.Repositories;
using System.Threading.Tasks;

namespace KBContact.Core
{
    public interface IUnitOfWork
    {
        IContactRepository Contacts { get; }
        ILabelsRepository Labels { get; }

        Task Complete();
    }
}