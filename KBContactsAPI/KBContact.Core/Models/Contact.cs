﻿namespace KBContact.Core.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Contact
    {
        [Required]
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(90, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Name { get; set; }
        [Required]
        [StringLength(90, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Surname { get; set; }
        [Required]
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public bool IsFavourite { get; set; }

        [ForeignKey("Label")]
        [Required]
        public int LabelId { get; set; }

        public Label Label { get; set; }
    }
}
