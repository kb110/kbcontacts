﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KBContact.Core.Models
{
    public class Label
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(90,ErrorMessage ="The {0} must be at least {1} characters long", MinimumLength = 2)]
        public string Name { get; set; }

        public ICollection<Contact> Contacts { get; set; }
    }
}
