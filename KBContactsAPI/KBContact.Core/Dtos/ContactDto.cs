﻿namespace KBContact.Core.Dtos
{
    using System.ComponentModel.DataAnnotations;
    public class ContactDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(90, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Name { get; set; }
        [Required]
        [StringLength(90, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Surname { get; set; }
        [Required]
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public int LabelId { get; set; }

        [Required]
        public bool IsFavourite { get; set; }
    }
}
