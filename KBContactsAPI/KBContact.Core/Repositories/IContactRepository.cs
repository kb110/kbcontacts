﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KBContact.Core.Models;

namespace KBContact.Core.Repositories
{
    public interface IContactRepository
    {
        void AddContact(Contact contact);
        bool ContactExists(int id);
        Task<Contact> GetContactByIdAsync(int id);
        Task<IEnumerable<Contact>> GetContactsAsync(string query = null);
        Task<IEnumerable<Contact>> GetContactsByLabelAsync(int labelId);
        void RemoveContact(Contact contact);
        void UpdateContact(Contact contact);
    }
}