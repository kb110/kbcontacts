﻿using KBContact.Core.Models;
using System.Data.Entity;

namespace KBContact.Core.Repositories
{
    public interface IKBContactDBContext
    {
        DbSet<Contact> Contacts { get; set; }
        DbSet<Label> Labels { get; set; }
    }
}
