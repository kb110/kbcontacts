﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KBContact.Core.Models;
using KBContacts.Core.DAO;

namespace KBContact.Core.Repositories
{
    public interface ILabelsRepository
    {
        void Add(Label label);
        Task<Label> GetLabelById(int id);
        Task<IEnumerable<LabelDAO>> GetLabels();
        bool LabelExists(int id);
        void RemoveLabel(Label label);
        void Update(Label label);
    }
}