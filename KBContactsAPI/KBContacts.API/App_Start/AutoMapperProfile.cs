﻿

namespace KBContact.API.App_Start
{
    using AutoMapper;
    using KBContact.Core.Dtos;
    using KBContact.Core.Models;

    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Contact, ContactDto>();
        }
    }
}