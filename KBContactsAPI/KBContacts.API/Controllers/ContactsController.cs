﻿using AutoMapper.QueryableExtensions;
using KBContact.Core;
using KBContact.Core.Dtos;
using KBContact.Core.Models;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace KBContact.API.Controllers
{
    public class ContactsController : ApiController
    {

        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;

        public ContactsController() { }

        public ContactsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Contacts
        [HttpGet, Route("api/Contacts")]
        [ResponseType(typeof(ContactDto))]
        public async Task<IEnumerable<ContactDto>> GetContacts(string query = null)
        {
            IEnumerable<Contact> contacts = new List<Contact>();
            try
            {
                contacts = await _unitOfWork.Contacts.GetContactsAsync(query);
                return contacts.AsQueryable().ProjectTo<ContactDto>();
            }
            catch (System.Exception e)
            {
                _log.Error(e.Message, e);
                throw;
            }
        }

        [HttpGet, Route("api/label/{labelId:int}/contacts")]
        [ResponseType(typeof(ContactDto))]
        public async Task<IHttpActionResult> GetContactsByLabel(int labelId)
        {
            if (labelId == 0)
            {
                IEnumerable<Contact> contacts = new List<Contact>();
                try
                {
                    contacts = await _unitOfWork.Contacts.GetContactsAsync();
                    return Ok(contacts.AsQueryable().ProjectTo<ContactDto>());
                }
                catch (System.Exception e)
                {
                    _log.Error(e.Message, e);
                    throw;
                }
            }
            else
            {
                IEnumerable<Contact> contacts = new List<Contact>();
                try
                {
                    contacts = await _unitOfWork.Contacts.GetContactsByLabelAsync(labelId);
                    return Ok(contacts.AsQueryable().ProjectTo<ContactDto>());
                }
                catch (System.Exception e)
                {
                    _log.Error(e.Message, e);
                    throw;
                }
            }
        }

        // Get: api/Contacts/Search
        [HttpGet, Route("api/Contacts/Search")]
        [ResponseType(typeof(Contact))]
        public IHttpActionResult Search(string searchTerm)
        {
            return RedirectToRoute("GetContacts", new { query = searchTerm });
        }

        // GET: api/Contacts/5
        [HttpGet, Route("api/Contacts/{id:int}")]
        [ResponseType(typeof(Contact))]
        public async Task<IHttpActionResult> GetContact(int id)
        {
            Contact contact = await _unitOfWork.Contacts.GetContactByIdAsync(id);
            if (contact == null)
            {
                _log.Warn($"Contact with id: {id} was not found");
                return NotFound();
            }

            return Ok(contact);
        }

        // PUT: api/Contacts/5
        [HttpPut, Route("api/Contacts/{id:int}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContact(int id, Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contact.Id)
            {
                _log.Warn($"The id provided by the application is not the same as the id of the object being tracked. tracked id: {contact.Id}, provided id: {id}");
                return BadRequest();
            }

            _unitOfWork.Contacts.UpdateContact(contact);

            try
            {
                await _unitOfWork.Complete();
            }
            catch (DbUpdateConcurrencyException e)
            {
                if (!ContactExists(id))
                {
                    _log.Warn($"Contact name: {contact.Name} with id: {id} is not in the database");
                    return NotFound();
                }
                else
                {
                    _log.Error(e.Message, e);
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }



        // POST: api/Contacts
        [HttpPost, Route("api/Contacts")]
        [ResponseType(typeof(Contact))]
        public async Task<IHttpActionResult> PostContact(Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _unitOfWork.Contacts.AddContact(contact);
                await _unitOfWork.Complete();
            }
            catch (System.Exception e)
            {
                _log.Error(e.Message, e);
                throw;
            }

            _log.Info($"New contact created - id: {contact.Id}, name: {contact.Name}");
            return CreatedAtRoute("DefaultApi", new { id = contact.Id }, contact);
        }

        // DELETE: api/Contacts/5
        [HttpDelete, Route("api/Contacts/{id:int}")]
        [ResponseType(typeof(Contact))]
        public async Task<IHttpActionResult> DeleteContact(int id)
        {
            Contact contact = await _unitOfWork.Contacts.GetContactByIdAsync(id);

            if (contact == null)
            {
                _log.Warn($"User tried to delete a contact that was not found in the database - id: {id}");
                return NotFound();
            }

            _unitOfWork.Contacts.RemoveContact(contact);

            await _unitOfWork.Complete();

            _log.Info($"Contact was deleted: {id}");
            return Ok(contact);
        }

        private bool ContactExists(int id)
        {
            return _unitOfWork.Contacts.ContactExists(id);
        }
    }
}