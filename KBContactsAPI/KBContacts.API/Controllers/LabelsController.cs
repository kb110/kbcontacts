﻿using KBContact.Core.Models;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity.Infrastructure;
using KBContact.Core;

namespace KBContacts.API.Controllers
{

    public class LabelsController : ApiController
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;

        public LabelsController() { }

        public LabelsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // api/Labels
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var model = await _unitOfWork.Labels.GetLabels();

            return Ok(model);
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            var model = await _unitOfWork.Labels.GetLabelById(id);

            if (model == null)
                return NotFound();

            return Ok(model);
        }

        [HttpPut]
        public async Task<IHttpActionResult> Put(int id, Label label)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (id != label.Id)
                return BadRequest();

            _unitOfWork.Labels.Update(label);

            try
            {
                await _unitOfWork.Complete();
            }
            catch (DbUpdateConcurrencyException e)
            {
                if (!LabelExist(id))
                {
                    _log.Warn($"Label with id: {id} is not in the database");
                    return NotFound();
                }
                else
                {
                    _log.Error(e.Message, e);
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post(Label label)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _unitOfWork.Labels.Add(label);

            await _unitOfWork.Complete();

            return CreatedAtRoute("DefaultApi", new { id = label.Id }, label);
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var label = await _unitOfWork.Labels.GetLabelById(id);

            if (label == null)
            {
                _log.Warn($"User tried to delete a label that was not found in the db - id: {id}");
                return NotFound();
            }

            _unitOfWork.Labels.RemoveLabel(label);

            await _unitOfWork.Complete();

            return Ok(label);
        }

        private bool LabelExist(int id)
        {
            return _unitOfWork.Labels.LabelExists(id);
        }
    }
}
