﻿using System;
using System.Threading.Tasks;
using KBContact.API.Controllers;
using KBContact.Core;
using KBContact.Core.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web.Http;

namespace KBContact.Tests
{
    [TestClass]
    public class ContactControllerTests
    {
        private ContactsController _controller;

        public ContactControllerTests()
        {
            var mockRepository = new Mock<IContactRepository>();

            var mockUoW = new Mock<IUnitOfWork>();
            mockUoW.SetupGet(u => u.Contacts).Returns(mockRepository.Object);

            _controller = new ContactsController(mockUoW.Object);

        }
        [TestMethod]
        public async Task DeleteContact_NoContactWithGivenIdExists_ShouldReturnNotFound()
        {
            var result = await _controller.DeleteContact(1);
        }
    }
}
