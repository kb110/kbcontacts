namespace KBContact.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsFavoriteToContact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "IsFavourite", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contacts", "IsFavourite");
        }
    }
}
