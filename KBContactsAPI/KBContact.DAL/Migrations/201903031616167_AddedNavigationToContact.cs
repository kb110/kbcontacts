namespace KBContact.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNavigationToContact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "LabelId", c => c.Int());
            CreateIndex("dbo.Contacts", "LabelId");
            AddForeignKey("dbo.Contacts", "LabelId", "dbo.Labels", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contacts", "LabelId", "dbo.Labels");
            DropIndex("dbo.Contacts", new[] { "LabelId" });
            DropColumn("dbo.Contacts", "LabelId");
        }
    }
}
