namespace KBContact.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLabelAndNavigationPropertiesToContact : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Labels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 90),
                        TotalContacts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Labels");
        }
    }
}
