namespace KBContact.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNavigationPropertiesToLabelAndContact : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Labels", "TotalContacts");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Labels", "TotalContacts", c => c.Int(nullable: false));
        }
    }
}
