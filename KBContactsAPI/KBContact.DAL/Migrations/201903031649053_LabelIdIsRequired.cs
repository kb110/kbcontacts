namespace KBContact.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LabelIdIsRequired : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contacts", "LabelId", "dbo.Labels");
            DropIndex("dbo.Contacts", new[] { "LabelId" });
            AlterColumn("dbo.Contacts", "LabelId", c => c.Int(nullable: false));
            CreateIndex("dbo.Contacts", "LabelId");
            AddForeignKey("dbo.Contacts", "LabelId", "dbo.Labels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contacts", "LabelId", "dbo.Labels");
            DropIndex("dbo.Contacts", new[] { "LabelId" });
            AlterColumn("dbo.Contacts", "LabelId", c => c.Int());
            CreateIndex("dbo.Contacts", "LabelId");
            AddForeignKey("dbo.Contacts", "LabelId", "dbo.Labels", "Id");
        }
    }
}
