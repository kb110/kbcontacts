﻿using KBContact.Core.Models;
using KBContact.Core.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace KBContact.DAL
{
    public class KBContactDBContext : IdentityDbContext<ApplicationUser>, IKBContactDBContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Label> Labels { get; set; }

        public KBContactDBContext()
            : base("name=kbcontacts", throwIfV1Schema: false)
        {

        }

        public static KBContactDBContext Create()
        {
            return new KBContactDBContext();
        }
    }
}