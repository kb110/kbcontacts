﻿using KBContact.Core;
using KBContact.Core.Repositories;
using KBContact.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KBContact.DAL.Persistance
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly KBContactDBContext _context;

        public IContactRepository Contacts { get; private set; }

        public ILabelsRepository Labels { get; private set; }

        public UnitOfWork(KBContactDBContext context)
        {
            _context = context;
            Contacts = new ContactRepository(context);
            Labels = new LabelsRepository(context);
        }

        public async Task Complete()
        {
           await _context.SaveChangesAsync();
        }
    }
}
