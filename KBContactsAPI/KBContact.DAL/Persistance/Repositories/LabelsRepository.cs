﻿using KBContact.Core.Models;
using KBContact.Core.Repositories;
using KBContacts.Core.DAO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KBContact.DAL.Repositories
{

    public class LabelsRepository : ILabelsRepository
    {
        private readonly KBContactDBContext _db;
        public LabelsRepository(KBContactDBContext kBContactDbContact)
        {
            _db = new KBContactDBContext();
        }

        public async Task<IEnumerable<LabelDAO>> GetLabels()
        {
            return await _db.Labels.AsQueryable().Select(x => new LabelDAO
            {
                Id = x.Id,
                Name = x.Name,
                ContactCount = _db.Contacts.Count(c => c.LabelId == x.Id)
            }).ToListAsync();
        }

        public async Task<Label> GetLabelById(int id)
        {
            return await _db.Labels.FindAsync(id);
        }

        public void Add(Label label)
        {
            _db.Labels.Add(label);
        }

        public void Update(Label label)
        {
            _db.Entry(label).State = System.Data.Entity.EntityState.Modified;
        }

        public bool LabelExists(int id)
        {
            return _db.Labels.Count(x => x.Id == id) > 0;
        }

        public void RemoveLabel(Label label)
        {
            _db.Labels.Remove(label);
        }
    }
}
