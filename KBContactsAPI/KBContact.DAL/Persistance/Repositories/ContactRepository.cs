﻿
namespace KBContact.DAL.Repositories
{
    using KBContact.Core.Models;
    using KBContact.Core.Repositories;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;

    public class ContactRepository : IContactRepository
    {
        private readonly KBContactDBContext _db;
        public ContactRepository(KBContactDBContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<Contact>> GetContactsAsync(string query = null)
        {
            if (!string.IsNullOrWhiteSpace(query))
            {
                return await _db.Contacts
                    .Where(c => c.Name == query || c.Surname == query || c.Email == query)
                    .ToListAsync();
            }
            return await _db.Contacts.ToListAsync();
        }

        public async Task<IEnumerable<Contact>> GetContactsByLabelAsync(int labelId)
        {
            return await _db.Contacts.Where(c => c.LabelId == labelId).ToListAsync();
        }

        public async Task<Contact> GetContactByIdAsync(int id)
        {
            return await _db.Contacts.FindAsync(id);
        }

        public void UpdateContact(Contact contact)
        {
            _db.Entry(contact).State = EntityState.Modified;
        }

        public void AddContact(Contact contact)
        {
            _db.Contacts.Add(contact);
        }

        public void RemoveContact(Contact contact)
        {
            _db.Contacts.Remove(contact);
        }

        public bool ContactExists(int id)
        {
            return _db.Contacts.Count(e => e.Id == id) > 0;
        }
    }

    //protected override void Dispose(bool disposing)
    //{
    //    if (disposing)
    //    {
    //        _db.Dispose();
    //    }
    //    base.Dispose(disposing);
    //}
}
