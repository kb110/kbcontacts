import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { Contact } from 'src/app/_models/contact';
import { Component, OnInit, Input } from '@angular/core';
import { ContactsService } from 'src/app/_services/contacts.service';

@Component({
	selector: 'app-favourite',
	templateUrl: './favourite.component.html',
	styleUrls: [ './favourite.component.css' ]
})
export class FavouriteComponent {
	@Input() isFavorite: boolean;
	@Input() contactId: number;
	@Input() contact: Contact;

	constructor(private _service: ContactsService, private snackBar: MatSnackBar) {}
	onClick() {
		this.isFavorite = !this.isFavorite;

		this.contact.isFavourite = this.isFavorite;
		
		this._service.updateContact(this.contactId, this.contact).subscribe(r => {
			console.log('like updated');
		},(e: HttpErrorResponse) => {
			this.snackBar.open('Failed to change status: ', 'Ok');
			console.log(e);
		});
	}
}
