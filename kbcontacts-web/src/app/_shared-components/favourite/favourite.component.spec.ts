import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteComponent } from './favourite.component';

describe('FavouriteComponent', () => {
	let component: FavouriteComponent;
	let fixture: ComponentFixture<FavouriteComponent>;

	beforeEach(
		async(() => {
			TestBed.configureTestingModule({
				declarations: [ FavouriteComponent ]
			}).compileComponents();
		})
	);

	beforeEach(() => {
		fixture = TestBed.createComponent(FavouriteComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should be true when liked', () => {
		component.isFavorite = false;
		component.onClick();
		expect(component.isFavorite).toBe(true);
	});

	it('should be false when unliked', () => {
		component.isFavorite = true;
		component.onClick();
		expect(component.isFavorite).toBe(false);
	});
});
