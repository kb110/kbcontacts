import { ContactListComponent } from './../_components/contact-list/contact-list.component';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Label } from './../_models/label';
import { LabelsService } from '../_services/labels.service';
import { ContactsService } from '../_services/contacts.service';

@Component({
	providers: [ ContactListComponent ],
	selector: 'app-main-nav',
	templateUrl: './main-nav.component.html',
	styleUrls: [ './main-nav.component.css' ]
})
export class MainNavComponent implements OnInit {
	labels = [] as Label[];

	isHandset$: Observable<boolean> = this.breakpointObserver
		.observe(Breakpoints.Handset)
		.pipe(map((result) => result.matches));

	@ViewChild(ContactListComponent) contactListComponent;

	constructor(
		private breakpointObserver: BreakpointObserver,
		private labelsService: LabelsService,
		private contactService: ContactsService
	) {}

	ngOnInit() {
		this.labelsService.getLabels().subscribe((x) => {
			this.labels = x.body;
		});
	}

	getContactsById(labelId: number) {
		this.contactService.getContactsByLabel(labelId);
	}

	
}
