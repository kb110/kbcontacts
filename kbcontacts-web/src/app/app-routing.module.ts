import { ContactDetailComponent } from './_components/contact-detail/contact-detail.component';
import { ContactFormComponent } from './_components/contact-form/contact-form.component';
import { ContactListComponent } from './_components/contact-list/contact-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{ path: 'list', component: ContactListComponent },
	{ path: 'create', component: ContactFormComponent },
	{ path: 'edit/:id', component: ContactFormComponent },
	{ path: 'detail/:id', component: ContactDetailComponent },
	{ path: '', redirectTo: '/list', pathMatch: 'full' }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule {}
