import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Label } from '../_models/label';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class LabelsService {

  baseUrl = environment.rootUrl;
  
  constructor(private _http: HttpClient) { }

  getLabels(): Observable<HttpResponse<Label[]>> {
		return this._http.get<Label[]>(`${this.baseUrl}/api/labels`, { observe: 'response' });
	}
	getLabelById(id: number): Observable<HttpResponse<Label>> {
		return this._http.get<Label>(`${this.baseUrl}/api/labels/${id}`, { observe: 'response' });
	}
	updateLabel(id: number, label: Label) {
		console.log(id);
		if (id === 0) return this._http.post(`${this.baseUrl}/api/labels`, label);
		else return this._http.put(`${this.baseUrl}/api/labels/${id}`, label);
	}
	deleteLabel(id: number) {
		return this._http.delete(`${this.baseUrl}/api/labels/${id}`);
	}
}
