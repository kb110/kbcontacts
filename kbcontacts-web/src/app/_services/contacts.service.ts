import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Contact } from '../_models/contact';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ContactsService {
	private baseUrl = environment.rootUrl;

	_contactsSubject = new BehaviorSubject<HttpResponse<Contact[]>>(new HttpResponse<Contact[]>());
	private contacts = this._contactsSubject.asObservable();

	constructor(private _http: HttpClient) {}

	getContactById(id: number): Observable<HttpResponse<Contact>> {
		return this._http.get<Contact>(`${this.baseUrl}/api/contacts/${id}`, { observe: 'response' });
	}
	updateContact(id: number, contact: Contact) {
		if (id === 0) {
			contact.id = 0;
			contact.isFavourite = false;
			return this._http.post(`${this.baseUrl}/api/contacts`, contact);
		} else {
			return this._http.put(`${this.baseUrl}/api/contacts/${id}`, contact);
		}
	}
	deleteContact(id: number) {
		return this._http.delete(`${this.baseUrl}/api/contacts/${id}`);
	}
	getContactsByLabel(labelId: number) {
		console.log('getContactsByLabelId is being called');
		if (labelId === 0) {
			console.log(labelId);
			this._http
				.get<Contact[]>(`${this.baseUrl}/api/contacts`, { observe: 'response' })
				.subscribe((x: HttpResponse<Contact[]>) => {
					this._contactsSubject.next(x);
				});
		} else {
			console.log(labelId);
			this._http
				.get(`${this.baseUrl}/api/label/${labelId}/contacts`, { observe: 'response' })
				.subscribe((c: HttpResponse<Contact[]>) => {
					this._contactsSubject.next(c);
				});
		}
	}
}
