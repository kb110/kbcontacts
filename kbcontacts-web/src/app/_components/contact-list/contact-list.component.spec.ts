import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactListComponent } from './contact-list.component';
import { ContactsService } from 'src/app/_services/contacts.service';
import { Observable, from } from 'rxjs';
import { Contact } from 'src/app/_models/contact';

describe('ContactListComponent', () => {
	let component: ContactListComponent;
	let fixture: ComponentFixture<ContactListComponent>;
	let service: ContactsService;

	beforeEach(
		async(() => {
			service = new ContactsService(null);
			TestBed.configureTestingModule({
				declarations: [ ContactListComponent ]
			}).compileComponents();
		})
	);

	beforeEach(() => {
		fixture = TestBed.createComponent(ContactListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should set contacts properties to contacts list from server', () => {
		let contacts = [ { id: 1 }, { id: 2 }, { id: 3 } ] as Contact[];
		spyOn(service, 'getContacts').and.callFake(() => {
			return from([ contacts ]);
		});

		component.ngOnInit();

		expect(component.contacts).toBe(contacts);
	});
});
