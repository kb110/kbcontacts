import { BehaviorSubject } from 'rxjs';
import { ConfirmationComponent } from './../../_shared-components/confirmation/confirmation.component';
import { ContactsService } from './../../_services/contacts.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar, MatDialog } from '@angular/material';
import { Contact } from 'src/app/_models/contact';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'app-contact-list',
	templateUrl: './contact-list.component.html',
	styleUrls: [ './contact-list.component.css' ]
})
export class ContactListComponent implements OnInit {
	displayedColumns: string[] = [ 'name', 'surname', 'email', 'favorite', 'actions' ];
	dataSource: MatTableDataSource<Contact>;
	isConfirmed = false;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	constructor(private service: ContactsService, private snackBar: MatSnackBar, public dialog: MatDialog) {}

	ngOnInit() {
		// initial loading of data
		this.service.getContactsByLabel(0);

		this.service._contactsSubject.subscribe((c) => {
			console.log(c.body);
			this.dataSource = new MatTableDataSource<Contact>(c.body);
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;
		});
	}
	deleteContact(id: number) {
		this.openDialog(id);
	}

	openDialog(id: number): void {
		const dialogRef = this.dialog.open(ConfirmationComponent, {
			width: '250px',
			disableClose: true,
			data: { isConfirmed: this.isConfirmed }
		});

		dialogRef.afterClosed().subscribe((result: boolean) => {
			// this.isConfirmed = result;
			if (result) {
				this.service.deleteContact(id).subscribe(
					(r) => {
						this.snackBar.open('Contact Deleted', 'Ok');
					},
					(err: HttpErrorResponse) => {
						this.snackBar.open(err.message, 'Ok');
						console.log(err);
					}
				);
			}
		});
	}

	// client side filtering
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}
}
