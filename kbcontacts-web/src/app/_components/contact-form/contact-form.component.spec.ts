import { FormBuilder } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactFormComponent } from './contact-form.component';

describe('ContactFormComponent', () => {
  let component: ContactFormComponent;
  let fixture: ComponentFixture<ContactFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('shoud create a form with 4 controls', () => {
    expect(component.contactForm.contains('id')).toBeTruthy();
    expect(component.contactForm.contains('name')).toBeTruthy();
    expect(component.contactForm.contains('surname')).toBeTruthy();
    expect(component.contactForm.contains('email')).toBeTruthy();
  })

  it('should make the 3 controls required', () => {
    let nameControl = component.contactForm.get('name');
    let surnameControl = component.contactForm.get('surname');
    let emailControl = component.contactForm.get('email');

    nameControl.setValue('');
    surnameControl.setValue('');
    emailControl.setValue('');

    expect(nameControl.valid).toBeFalsy();
    expect(surnameControl.valid).toBeFalsy();
    expect(emailControl.valid).toBeFalsy();
  })
});
