import { ContactsService } from './../../_services/contacts.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Contact } from 'src/app/_models/contact';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

@Component({
	selector: 'app-contact-form',
	templateUrl: './contact-form.component.html',
	styleUrls: [ './contact-form.component.css' ]
})
export class ContactFormComponent implements OnInit {
	isLoading = false;
	contactForm: FormGroup;
	contact = {} as Contact;
	submitted = false;
	private _id: number;

	get name() {
		return this.contactForm.get('name');
	}
	get surname() {
		return this.contactForm.get('surname');
	}
	get email() {
		return this.contactForm.get('email');
	}
	constructor(
		private contactService: ContactsService,
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private snackBar: MatSnackBar
	) {}

	ngOnInit() {
		this._id = +this.route.snapshot.paramMap.get('id');

		// we are getting this id from route data - maybe we should treat it as being dangerous
		if (this._id) this.getContact(this._id);

		this.contactForm = this.formBuilder.group({
			id: [],
			name: [ '', [ Validators.required, Validators.minLength(2), Validators.maxLength(90) ] ],
			surname: [ '', [ Validators.required, Validators.minLength(2), Validators.maxLength(90) ] ],
			email: [ '', [ Validators.required, Validators.email ] ]
		});
	}

	onSubmit(): void {
		this.submitted = true;

		if (this.contactForm.invalid) return;

		this.isLoading = true;

		// update contact, otherwise create new contact
		this.contactService.updateContact(this._id, this.contactForm.value).subscribe(
			(res) => {
				this.snackBar.open('Contact Saved', 'Ok');
				this.isLoading = false;
				this.router.navigate(['/list']);
			},
			(err: HttpErrorResponse) => {
				this.snackBar.open(err.message, 'Ok');
				console.log(err);
				this.isLoading = false;
			}
		);
	}

	getContact(id: number) {
		this.isLoading = true;
		this.contactService.getContactById(id).subscribe(
			(c) => {
				this.editContact(c.body);
				this.isLoading = false;
			},
			(err: HttpErrorResponse) => {
				this.snackBar.open(err.message, 'Ok');
				console.log(err.message);
			}
		);
	}

	editContact(contact: Contact) {
		this.contactForm.patchValue({
			id: this._id,
			name: contact.name,
			surname: contact.surname,
			email: contact.email
		});
	}
}
