import { MatSnackBar } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { ContactsService } from './../../_services/contacts.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Contact } from 'src/app/_models/contact';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {

  private _id: number;
  _contact = {} as Contact;

  constructor(private service: ContactsService, private route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this._id = +this.route.snapshot.paramMap.get('id');
    if (this._id) {
      // It looks like the labelId of this contact does not get passed

      this.service.getContactById(this._id).subscribe(c => {
        this._contact = c.body;
        console.log(c.body);
      },(e: HttpErrorResponse) => {
        this.snackBar.open(e.message, 'Ok');
        console.log(e);
      });
    }
  }
}
