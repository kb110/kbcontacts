export interface Contact {
	isFavourite: boolean;
	id: number;
	labelId: number;
	name: string;
	surname: string;
	email: string;
}
