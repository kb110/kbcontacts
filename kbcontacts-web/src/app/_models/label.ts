
export interface Label {
	id: number;
	name: string;
	contactCount: number;
}
