import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatTableModule, MatPaginatorModule, MatMenuModule, MatCardModule, MatProgressBarModule, MatSnackBarModule, MatFormFieldModule, MatInputModule, MatDialogModule } from '@angular/material';
import { ContactListComponent } from './_components/contact-list/contact-list.component';
import { ContactFormComponent } from './_components/contact-form/contact-form.component';
import { ConfirmationComponent } from './_shared-components/confirmation/confirmation.component';
import { FavouriteComponent } from './_shared-components/favourite/favourite.component';
import { ContactDetailComponent } from './_components/contact-detail/contact-detail.component';
import { ContactsService } from './_services/contacts.service';


@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    ContactListComponent,
    ContactFormComponent,
    ConfirmationComponent,
    FavouriteComponent,
    ContactDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LayoutModule
  ],
  providers: [ContactsService],
  bootstrap: [AppComponent],
  entryComponents:[
    ConfirmationComponent
  ]
})
export class AppModule { }
